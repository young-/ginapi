package model

import (
	"api/pkg/auth"
	"api/pkg/constvar"
	"fmt"
	"gopkg.in/go-playground/validator.v9"
)

type UserModel struct {
	BaseModel
	Username string `json:"username" gorm:"colum:username;not noll" binding:"required" validate:"min=1,max=32"`
	Password string `json:"password" gorm:"colum:password;not noll" binding:"required" validate:"min=1,max=128"`
}

func (c *UserModel) TableName() string {
	return "tb_users"
}

//添加用户
func (u *UserModel) Create() error {
	return DB.Self.Create(&u).Error
}

//修改用户
func (u *UserModel) Update(m interface{}) error {
	return DB.Self.Model(u).Updates(m).Error
}

//删除用户
func DeleteUser(id uint64) error {
	user := UserModel{}
	user.BaseModel.Id = id
	return DB.Self.Delete(&user).Error
}

//验证用户
func (u *UserModel) Validate() (err error) {
	validate := validator.New()
	return validate.Struct(u)
}

//加密密码
func (u *UserModel) Encrypt() (err error) {
	u.Password, err = auth.Encrypt(u.Password)
	return
}

//用户列表
func ListUser(username string, offset, limit int) ([]*UserModel, uint64, error) {
	if limit == 0 {
		limit = constvar.DefaultLimit
	}

	users := make([]*UserModel, 0)
	var count uint64

	where := fmt.Sprintf("username like '%%%s%%'", username)
	if err := DB.Self.Model(&UserModel{}).Where(where).Count(&count).Error; err != nil {
		return users, count, err
	}

	if err := DB.Self.Where(where).Offset(offset).Limit(limit).Order("id desc").Find(&users).Error; err != nil {
		return users, count, err
	}

	return users, count, nil
}

// 根据用户名获取用户
func GetUser(username string) (*UserModel, error) {
	u := &UserModel{}
	d := DB.Self.Where("username = ?", username).First(&u)
	return u, d.Error
}
