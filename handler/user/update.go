package user

import (
	. "api/handler"
	"api/model"
	"api/pkg/errno"
	"api/util"
	"github.com/gin-gonic/gin"
	"github.com/lexkong/log"
	"github.com/lexkong/log/lager"
	"strconv"
)

func Update(c *gin.Context) {
	log.Info("Update function called.", lager.Data{"X-Request-Id": util.GetReqID(c)})
	userId, _ := strconv.Atoi(c.Param("id"))
	//绑定参数
	var u model.UserModel
	if err := c.Bind(&u); err != nil {
		SendResponse(c, errno.ErrBind, nil)
		return
	}
	u.Id = uint64(userId)
	//验证参数
	if err := u.Validate(); err != nil {
		SendResponse(c, errno.ErrValidation, nil)
		return
	}

	//加密密码
	if err := u.Encrypt(); err != nil {
		SendResponse(c, errno.ErrEncrypt, nil)
		return
	}

	SmartPrint(u.BaseModel)
	SmartPrint(u)

	//修改的数据
	m := make(map[string]interface{})
	m["username"] = u.Username
	m["password"] = u.Password

	//修改数据
	if err := u.Update(m); err != nil {
		log.Debugf("sql运行错误：%s", err)
		SendResponse(c, errno.ErrDatabase, nil)
		return
	}

	SendResponse(c, nil, nil)
}
