package user

import (
	. "api/handler"
	"api/model"
	"github.com/gin-gonic/gin"
	"strconv"
	"tapi-blog/pkg/errno"
)

//删除用户
func Delete(c *gin.Context) {
	userId, _ := strconv.Atoi(c.Param("id"))
	if err := model.DeleteUser(uint64(userId)); err != nil {
		SendResponse(c, errno.ErrDatabase, nil)
		return
	}

	SendResponse(c, nil, nil)

}
